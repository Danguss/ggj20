﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCollider : MonoBehaviour
{
    void OnTriggerEnter2D (Collider2D col) {
        if(col.gameObject.tag == "tile")
            GameManagerScript.instance.isGameOver = true;
    }
}
