﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardJitter : MonoBehaviour
{
    [SerializeField] private float speed = 200f;

    void Update()
    {
        gameObject.transform.localPosition += Vector3.up * Mathf.Sin(Time.time * speed) * 0.001f;
    }
}
