﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleController : VehicleBase
{
  [SerializeField] private Transform vehicleTransform = null;
  [SerializeField] private AudioSource sound = null;
  [SerializeField] private float bikeSpeed = 10f;
  private float currentRotation = 0f;
  private Transform currentPivot;

  void Start () {
    currentPivot = backEngine;
  }

  void Update()
  { 
    if (!GameManagerScript.instance.GamePaused && !sound.isPlaying) {
      sound.Play();
    } 
    if(GameManagerScript.instance.GamePaused)
      sound.Stop();
    else {
      base.Update();
      TiltingVehicle();
      backEngine.Rotate(Vector3.forward * Time.deltaTime * -bikeSpeed*3);
      frontEngine.Rotate(Vector3.forward * Time.deltaTime * -bikeSpeed*3);
      vehicleTransform.RotateAround(currentPivot.position, Vector3.forward, currentRotation);
      vehicleTransform.position += Vector3.right * Time.deltaTime * bikeSpeed;
    }
  }

  private void TiltingVehicle () {
    if (Input.GetKey(KeyCode.LeftArrow))
    {
      currentRotation += Time.deltaTime * 10;
      currentPivot = backEngine;
    } else if (Input.GetKey(KeyCode.RightArrow)) {
      currentRotation -= Time.deltaTime * 10;
      currentPivot = frontEngine;
    } else {
      currentRotation = Mathf.Lerp(currentRotation, 0f, 0.5f);
    }
  }
  
}
