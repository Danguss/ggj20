﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : VehicleBase
{
    [SerializeField] private float RocketSpeed = 10f;
    [SerializeField] private AudioSource sound;
    void Update()
    {
        if(!GameManagerScript.instance.GamePaused && !sound.isPlaying)
            sound.Play();
        if(GameManagerScript.instance.GamePaused)
            sound.Stop();
        else {
            base.Update();
            if (Input.GetKey(KeyCode.LeftArrow)) {
                gameObject.transform.position += Vector3.left * Time.deltaTime * 3;
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * 10f);    
            } else if (Input.GetKey(KeyCode.RightArrow)) {
                gameObject.transform.position += Vector3.right * Time.deltaTime * 3;
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * -10f);     
            } else {
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * 0);
            }
            
            gameObject.transform.position += Vector3.up * Time.deltaTime * RocketSpeed;
            if(!frontEngine.gameObject.activeSelf) {
                gameObject.transform.position += Vector3.right * Time.deltaTime * 3;
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * -10f); 
            }
            if(!backEngine.gameObject.activeSelf) {
                gameObject.transform.position += Vector3.left * Time.deltaTime * 3;
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * 10f);   
            }
        }
    }
}
