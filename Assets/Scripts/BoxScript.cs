﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScript : MonoBehaviour
{
    public Rigidbody2D _rb;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // hardcodinam
        if (other.gameObject.name == "Plane" || other.gameObject.name == "Rocket")
        {
            _rb.isKinematic = false;
        }
    }
}
