﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : VehicleBase
{
    [SerializeField] private float planeSpeed = 10f;
    [SerializeField] private AudioSource sound;
    void Update()
    {
        if(!GameManagerScript.instance.GamePaused && !sound.isPlaying)
            sound.Play();
        if(GameManagerScript.instance.GamePaused)
            sound.Stop();
        else {
            base.Update();
            if (Input.GetKey(KeyCode.UpArrow)) {
                gameObject.transform.position += Vector3.up * Time.deltaTime * 3;
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * 10f);
            } else if (Input.GetKey(KeyCode.DownArrow)) {
                gameObject.transform.position += Vector3.down * Time.deltaTime * 3;    
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * -10f);
            } else { 
                gameObject.transform.rotation = Quaternion.Euler(Vector3.forward * 0);
            }
            
            gameObject.transform.position += Vector3.right * Time.deltaTime * planeSpeed;
        }    
    }
}
