﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Text Logo2;

    void Start()
    {
        InvokeRepeating("Blink", 1f, 1f);
    }

    public void StartGame ()
    {
        SceneManager.LoadScene("Level_1");
    }
    public void Exit()
    {
        Application.Quit();
    }

    private void Blink()
    {
        Logo2.gameObject.SetActive(!Logo2.gameObject.activeSelf);
    }
}
