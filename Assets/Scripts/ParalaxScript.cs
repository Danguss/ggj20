﻿using UnityEngine;

public class ParalaxScript : MonoBehaviour
{
	public float ScrollSpeedX = 0.003f;
	public float ScrollSpeedY = 0.003f;
	public Transform Camera;
	private Vector2 _savedOffset;
	private Renderer _renderer;

	void Start()
	{
		_renderer = GetComponent<Renderer>();
		_savedOffset = _renderer.sharedMaterial.GetTextureOffset("_MainTex");
	}

	void Update()
	{
		var x = _savedOffset.x;
		var y = _savedOffset.y;

		if (Camera != null)
		{
			x = Mathf.Repeat(_savedOffset.x + Camera.position.x * ScrollSpeedX, 1f);
			y = Mathf.Repeat(_savedOffset.y + Camera.position.y * ScrollSpeedY, 1f);
		}

		_renderer.sharedMaterial.SetTextureOffset("_MainTex", new Vector2(x, y));
	}

	void OnDisable()
	{
		_renderer.sharedMaterial.SetTextureOffset("_MainTex", _savedOffset);
	}
}