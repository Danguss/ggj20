﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    public int SectionCountDownSeconds = 60;
    public GameState State = GameState.Bike;

    public Text PauseText;
    public GameObject ExplanationPanel;
    public GameObject YouWinPanel;

    public GameObject YouLosePanel;
    public Text CountDownText;

    public bool GamePaused { get; set; } = false;

    public bool isGameOver { get; set; } = false;
    public GameObject Bike1;

    public GameObject Plane;

    public GameObject Rocket;

    public SimpleCameraScript Camera;

    public static GameManagerScript instance = null;

    int _countDown = 999;

    private bool _firstTimeRun = true;
    private bool _youWin = false;

    bool _stopCountDown = false;

    public GameObject PuzzlePanel;
    public Text PuzzleText;

    public GameObject SolutionPanel;

    public Text SolutionText;

    public GameObject SolvedPanel;

    public GameObject StarsBg1;
    public GameObject StarsBg2;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            _countDown = SectionCountDownSeconds;
            _firstTimeRun = true;
            PauseGame();
            PauseText.gameObject.SetActive(false);
            ExplanationPanel.SetActive(true);
            CountDownText.text = _countDown.ToString();

            PuzzlePanel.SetActive(false);
            SolutionPanel.SetActive(false);
            PuzzleText.text = string.Empty;
            SolutionText.text = string.Empty;
            SolvedPanel.SetActive(false);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown (KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
        else if (Input.GetKeyDown (KeyCode.P)) 
        {
            if (Time.timeScale == 0)
            {
                if (_firstTimeRun || _youWin || isGameOver)
                {
                    return;
                }
                ContinueGame();
            }
            else
            {
                PauseGame();
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_firstTimeRun)
            {
                _firstTimeRun = false;
                ExplanationPanel.SetActive(false);
                ContinueGame();

                CountDownText.text = _countDown.ToString();
                InvokeRepeating("CountDown", 1f, 1f);
            }
            if (_youWin || isGameOver)
            {
                // OR some credits roll???
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
        GamePaused = true;
        PauseText?.gameObject.SetActive(true);
    } 
    private void ContinueGame()
    {
        Time.timeScale = 1;
        GamePaused = false;
        PauseText?.gameObject.SetActive(false);
    }

    private void CountDown()
    {
        if (isGameOver)
        {
            PauseGame();
            PauseText.gameObject.SetActive(false);
            YouLosePanel.SetActive(true);
        }

        if (Time.timeScale == 0 || CountDownText == null || _stopCountDown)
        {
            return;
        }

        _countDown--;

        if (_countDown < 0)
        {
            if (State == GameState.Rocket)
            {
                // you win, bro!!!
                PauseGame();
                PauseText.gameObject.SetActive(false);
                YouWinPanel.SetActive(true);
                _youWin = true;
                return;
            }

            _stopCountDown = true;
            _countDown = SectionCountDownSeconds;
            
            if (State == GameState.Bike)
            {
                Debug.Log("Game state PLANE");
                State = GameState.Plane;
                CountDownText.color = new Color(1/255f*237f, 1/255f*127f, 1/255f*45f);
            }
            else if (State == GameState.Plane)
            {
                Debug.Log("Game state ROCKET");
                State = GameState.Rocket;
                CountDownText.color = new Color(1/255f*222f, 1/255f*31f, 1/255f*41f);
            }
            else
            {
                Debug.Log("this should not happen!");
            }
        }

        CountDownText.text = _countDown.ToString();

        if (State == GameState.Plane && _stopCountDown)
        {
            CountDownText.text = "GO GO GO, Plane is ready!";
        }
        else if (State == GameState.Rocket && _stopCountDown)
        {
            CountDownText.text = "Rocket launch engaged!";
        }
    }

    public void SwitchToPlane()
    {
        _stopCountDown = false;
        Plane.transform.position = Bike1.transform.position;
        Bike1.SetActive(false);
        Plane.SetActive(true);
        Camera.Target = Plane.transform;
        Camera.SetPlaneMode();
    }

    public void SwitchToRocket()
    {
        _stopCountDown = false;
        Rocket.transform.position = Plane.transform.position;
        Plane.SetActive(false);
        Rocket.SetActive(true);
        Camera.Target = Rocket.transform;
        StarsBg1.SetActive(true);
        StarsBg2.SetActive(true);
    }

    public void EnterSolution(string key)
    {
        Debug.Log($"Entering solution {key}");
        if (SolutionText.text.Length >= PuzzleText.text.Length)
        {
            return;
        }
        SolutionText.text += key;
    }

    public void CreateOrIncreaseSolution(SolutionDifficulty difficulty)
    {
        PuzzlePanel.SetActive(true);
        SolutionPanel.SetActive(true);

        switch (difficulty)
        {
            case SolutionDifficulty.Medium:
                PuzzleText.text += PuzzleText.text.Length < 4 ? GetRandomCharacter(difficulty) : string.Empty;
                PuzzleText.text += PuzzleText.text.Length < 4 ? GetRandomCharacter(difficulty) : string.Empty;
                break;
            case SolutionDifficulty.Hard:
                PuzzleText.text += PuzzleText.text.Length < 6 ? GetRandomCharacter(difficulty) : string.Empty;
                PuzzleText.text += PuzzleText.text.Length < 6 ? GetRandomCharacter(difficulty) : string.Empty;
                break;
            default:
                PuzzleText.text += PuzzleText.text.Length < 2 ? GetRandomCharacter(difficulty) : string.Empty;
                PuzzleText.text += PuzzleText.text.Length < 2 ? GetRandomCharacter(difficulty) : string.Empty;
                break;
        }

        Debug.Log($"Created puzzle {PuzzleText.text}");
    }

    private string GetRandomCharacter(SolutionDifficulty difficulty)
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        string chars = difficulty == SolutionDifficulty.Hard ? "QWER" : "QWE";
        int index = Random.Range(0, chars.Length);
        return chars[index].ToString();
    }

    public bool ValidateSolution()
    {
        bool result = SolutionText.text != string.Empty && SolutionText.text.Equals(PuzzleText.text);

        if (!result && !PuzzleText.text.StartsWith(SolutionText.text))
        {
            Invoke("EmptySolution", 0.5f);
        }

        if (result)
        {
            PuzzleText.text = string.Empty;
            SolutionText.text = string.Empty;
            PuzzlePanel.SetActive(false);
            SolutionPanel.SetActive(false);
            SolvedPanel.SetActive(true);
            Invoke("DisableSolved", 1);
        }
        return result;
    }

    private void EmptySolution()
    {
        SolutionText.text = string.Empty;
    }

    private void DisableSolved()
    {
        SolvedPanel.SetActive(false);
    }
}

// omfg two classes in same file, so evil ~~~
public enum GameState
{
    Bike,
    Plane,
    Rocket
}

public enum SolutionDifficulty
{
    Easy,
    Medium,
    Hard
}
