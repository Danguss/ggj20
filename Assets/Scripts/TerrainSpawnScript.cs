﻿using UnityEngine;

public class TerrainSpawnScript : MonoBehaviour
{
	public GameObject BikeStartTile;

	public GameObject[] BikeTiles;

	public GameObject PlaneStartTile;

	public GameObject[] PlaneTiles;

	public GameObject RocketStartTile;

	public GameObject[] RocketTiles;

	public float StartSpawnRelativeX = -15;
	public float LevelSizeX = 40f;
	public float LevelSizeY = 40f;
	private int _randomChoice = -1;
	private int _lastChoice = -1;
	private float _lastXPos;
	private float _lastYPos = 0;
	float _currentLastTileX;
	bool _canSpawn = true;

	bool _bikeStartSpawned = false;
	bool _planeStartSpawned = false;
	bool _rocketStartSpawned = false;
	
	void Start()
	{
		_lastXPos = gameObject.transform.position.x + StartSpawnRelativeX;
	}
	
	void Update()
	{
		if (_canSpawn == true && (gameObject.transform.position.x >= _lastXPos || GameManagerScript.instance.State == GameState.Rocket && gameObject.transform.position.y >= _lastYPos))
		{
			_canSpawn = false;
			GameObject selected = null;
			_randomChoice = -1;

			if (GameManagerScript.instance.State == GameState.Bike)
			{
				if (!_bikeStartSpawned)
				{
					selected = BikeStartTile;
					_bikeStartSpawned = true;
				}
				else
				{
            		// do not select last one
					while (_lastChoice == _randomChoice || _randomChoice == -1)
					{
						_randomChoice = Random.Range(0, BikeTiles.Length);
					}

					selected = BikeTiles[_randomChoice];
				}
			}
			else if (GameManagerScript.instance.State == GameState.Plane)
			{
				if (!_planeStartSpawned)
				{
					selected = PlaneStartTile;
					_planeStartSpawned = true;
				}
				else
				{
            		// do not select last one
					while (_lastChoice == _randomChoice || _randomChoice == -1)
					{
						_randomChoice = Random.Range(0, PlaneTiles.Length);
					}

					selected = PlaneTiles[_randomChoice];
				}
			}
			else if (GameManagerScript.instance.State == GameState.Rocket)
			{
				if (!_rocketStartSpawned)
				{
					selected = RocketStartTile;
					_rocketStartSpawned = true;
					// spaghetti code
					SpawnTerrain(selected, false);
					return;
				}
				else
				{
            		// do not select last one
					while (_lastChoice == _randomChoice || _randomChoice == -1)
					{
						_randomChoice = Random.Range(0, RocketTiles.Length);
					}

					selected = RocketTiles[_randomChoice];
				}
			}
			else
			{
				Debug.Log("Should not happen!!!");
			}
			
			_lastChoice = _randomChoice;
			SpawnTerrain(selected, GameManagerScript.instance.State == GameState.Rocket);
		}
	}
	
	void SpawnTerrain(GameObject tile, bool isRocket = false)
	{
		if (!isRocket)
		{
			Instantiate(tile, new Vector3(_lastXPos, _lastYPos, 0), Quaternion.Euler(0, 0, 0));
			_lastXPos += LevelSizeX;
			_canSpawn = true;
			return;
		}

		Instantiate(tile, new Vector3(_lastXPos - LevelSizeX, _lastYPos + LevelSizeY, 0), Quaternion.Euler(0, 0, 0));
		_lastYPos += LevelSizeY;
		_canSpawn = true;
	}
}
