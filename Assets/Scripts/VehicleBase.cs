﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VehicleBase : MonoBehaviour
{
    [SerializeField] private GameObject wheelPrefab = null;
    [SerializeField] private GameObject lowGas = null;
    [SerializeField] private GameObject hitDamage = null;
    [SerializeField] public Transform frontEngine = null;
    [SerializeField] public Transform backEngine = null;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float jitterAmount = 0.001f;
    private bool isBackEngineBreaking;
    private bool isFrontEngineBreaking;
    private bool isBackEngineBroken = false;
    private bool isFrontEngineBroken = false;
    private bool engineCoroutineLock = false;
    private bool gasCoroutineLock = false;
    private bool isfalling = false;
    private bool canFixEngine = false;
    private bool canRefillGas = false;
    private bool fixDamage = false;
    private bool isGasTimeStarted = false;

    private IEnumerator blinkLowGas;
    private IEnumerator blinkHitDamage;
    private int gasTime = 0;
    void OnEnable() {
      blinkLowGas = BlinkIcon(lowGas);
      blinkHitDamage = BlinkIcon(hitDamage);
    }
    public void Update()
    {
        BreakEngine();
        EmptyGasTank();

        HandleBreaking(isFrontEngineBreaking, frontEngine);
        HandleBreaking(isBackEngineBreaking, backEngine);
        HandleGasEmpty();

        if (canFixEngine || canRefillGas || isfalling)
        {
            int howMany = new bool[3]{canFixEngine, canRefillGas, isfalling}.Count(c => c);

            if (howMany <= 1)
            {
                GameManagerScript.instance.CreateOrIncreaseSolution(SolutionDifficulty.Easy);
            }
            else if (howMany <= 2)
            {
                GameManagerScript.instance.CreateOrIncreaseSolution(SolutionDifficulty.Medium);
            }
            else
            {
                GameManagerScript.instance.CreateOrIncreaseSolution(SolutionDifficulty.Hard);
            }

            bool result = false;
            if (Input.GetKeyDown(KeyCode.Q))
            {
                GameManagerScript.instance.EnterSolution("Q");
                result = GameManagerScript.instance.ValidateSolution();
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                GameManagerScript.instance.EnterSolution("W");
                result = GameManagerScript.instance.ValidateSolution();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                GameManagerScript.instance.EnterSolution("E");
                result = GameManagerScript.instance.ValidateSolution();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                GameManagerScript.instance.EnterSolution("R");
                result = GameManagerScript.instance.ValidateSolution();
            }

            if (result)
            {
                FixEngine();
                refillGas();
                if (isfalling)
                {
                    isfalling = false;
                    StopCoroutine(blinkHitDamage);
                    hitDamage.SetActive(false);
                }
            }
        }
        // if (canFixEngine && Input.GetKey(KeyCode.Q))
        // {
        //     FixEngine();
        // }

        // if (canRefillGas && Input.GetKey(KeyCode.E))
        // {
        //     refillGas();
        // }
        
        if (isfalling)
        {
          gameObject.transform.position -= Vector3.up * Time.deltaTime * 2f;
          StartCoroutine(blinkHitDamage);
        }
        // if (isfalling && Input.GetKey(KeyCode.R))
        // {
        //   isfalling = false;
        //   StopCoroutine(blinkHitDamage);
        //   hitDamage.SetActive(false);
        // }
    }
    private void HandleBreaking(bool isBreaking, Transform pivot)
    {
        if (isBreaking && pivot != null)
        {
            StartBreakingEngine(pivot);
            if (!engineCoroutineLock)
            {
                engineCoroutineLock = true;
                StartCoroutine("EngineBrakeEvent");
            }
        }
    }

    private void HandleGasEmpty()
    {
        if (isGasTimeStarted)
        {
            if (!gasCoroutineLock)
            {
                gasCoroutineLock = true;
                StartCoroutine("LowGasEvent");
            }
        }
    }

    private void BreakEngine()
    {
        var wheel = UnityEngine.Random.Range(1, 3);
        var brakeEventNumber = UnityEngine.Random.Range(1, 2000);
        if (brakeEventNumber < 10 && !isBackEngineBreaking && !isFrontEngineBreaking)
        {
            if (wheel == 1)
            {
                isBackEngineBreaking = !isBackEngineBroken;
                Debug.Log("back breaking");
            }
            else
            {
                if (frontEngine != null)
                {
                    isFrontEngineBreaking = !isFrontEngineBroken;
                    Debug.Log("front breaking");
                }
            }
        }
    }

    private void EmptyGasTank()
    {
        if (!isGasTimeStarted)
        {
            gasTime = UnityEngine.Random.Range(6, 9);
            isGasTimeStarted = true;
        }
    }

    private void StartBreakingEngine(Transform engine)
    {
        engine.localPosition += Vector3.up * Mathf.Sin(Time.time * speed * 2f) * jitterAmount;
    }

    private void refillGas()
    {
        StopCoroutine("LowGasEvent");
        StopCoroutine(blinkLowGas);
        lowGas.SetActive(false);
        canRefillGas = false;
        gasCoroutineLock = false;
    }

    private void FixEngine()
    {
        if (frontEngine != null  && !frontEngine.gameObject.activeSelf)
            frontEngine.gameObject.SetActive(true);
        if (!backEngine.gameObject.activeSelf)
            backEngine.gameObject.SetActive(true);
        isBackEngineBroken = false;
        isFrontEngineBroken = false;
        canFixEngine = false;
    }

    private void OnCollisionEnter2D (Collision2D col) {
      if(col.collider.gameObject.tag == "box") {
        isfalling = true;
      }
    }

    private IEnumerator EngineBrakeEvent()
    {
        yield return new WaitForSeconds(5f);
        canFixEngine = true;
        var isBack = isBackEngineBreaking;
        if (isFrontEngineBreaking)
        {
            Debug.Log("Front broke");
            isFrontEngineBreaking = false;
            isFrontEngineBroken = true;
            frontEngine.gameObject.SetActive(false);
        }
        if (isBackEngineBreaking)
        {
            Debug.Log("Back broke");
            isBackEngineBreaking = false;
            isBackEngineBroken = true;
            backEngine.gameObject.SetActive(false);
        }
        Instantiate(wheelPrefab, isBack ? backEngine.position : frontEngine.position, Quaternion.identity);
        engineCoroutineLock = false;
    }
    private IEnumerator LowGasEvent()
    {
        yield return new WaitForSeconds(gasTime - 4);
        canRefillGas = true;
        StartCoroutine(blinkLowGas);
        yield return new WaitForSeconds(4f);
        gasCoroutineLock = false;
        GameManagerScript.instance.isGameOver = true;
        StopCoroutine(blinkLowGas);
        lowGas.SetActive(false);
    }

    private IEnumerator BlinkIcon(GameObject icon)
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            icon.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            icon.SetActive(false);
        }
    }
}
