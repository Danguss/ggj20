﻿using UnityEngine;
using System;

public class SimpleCameraScript : MonoBehaviour
{
    public float DampTime = 0.15f;
    public Transform Target;
    public float OffsetY;
    private Vector3 _velocity = Vector3.zero;
    private Camera _camera;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _camera.orthographicSize = 6;
    }

    void Update()
    {
        DoCameraIteration();
    }

    public void SetPlaneMode()
    {
        _camera.orthographicSize = 8;
    }

    private void DoCameraIteration()
    {
        if (Target)
        {
            Vector3 point = _camera.WorldToViewportPoint(Target.position);
            Vector3 delta = Target.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f + OffsetY, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref _velocity, DampTime);
        }
        else
        {
            throw new Exception("Add hero to camera, fool!");
        }
    }
}