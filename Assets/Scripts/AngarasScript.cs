﻿using UnityEngine;

public class AngarasScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        // hardcodinam
        if (other.tag == "Player")
        {
            GameManagerScript.instance.SwitchToPlane();
        }
    }
}
