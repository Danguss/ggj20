﻿using UnityEngine;

public class TerrainDestroyScript : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.name == "TerrainDestroyCollider")
		{
			Destroy(gameObject);
		}
	}

	private void OnDestroy ()
	{
		if (transform.parent != null)
		{
			if (transform.parent.childCount <= 1)
			{
				Destroy (transform.parent.gameObject, 0.1f);
			}
		}
	}
}