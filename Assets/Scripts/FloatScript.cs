﻿using UnityEngine;

public class FloatScript : MonoBehaviour
{
    public float frequencyMin = 1.0f;
    public float frequencyMax = 2.0f;
    public float magnitude = 0.0025f;
    private float randomInterval;

    void Start()
    {
        randomInterval = Random.Range(frequencyMin, frequencyMax);
    }

    void Update()
    {
        if (Time.timeScale > 0)
        {
            transform.position += new Vector3(0.0f, Mathf.Cos(Time.time * randomInterval) * magnitude, 0.0f);
            transform.eulerAngles += new Vector3(0.0f, 0.0f, (Mathf.Cos(Time.time * randomInterval)) / 6);
        }
    }
}