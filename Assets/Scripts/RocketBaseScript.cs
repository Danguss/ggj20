﻿using UnityEngine;

public class RocketBaseScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        // hardcodinam
        if (other.tag == "Player")
        {
            GameManagerScript.instance.SwitchToRocket();
        }
    }
}
